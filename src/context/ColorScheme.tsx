import React, {
  createContext,
  useState,
  ReactNode,
  Dispatch,
  SetStateAction,
  useContext,
} from "react";
import {
  useColorScheme as useRNColorScheme,
  ColorSchemeName as RNColorSchemeName,
} from "react-native";

export type ColorSchemeName =
  | RNColorSchemeName
  | "hyggeHome"
  | "friendlyRose"
  | "violetDream"
  | "technoParty"
  | "nightStorm"
  | "seaSide";

interface ColorSchemeContextProps {
  colorScheme: ColorSchemeName;
  setColorScheme: Dispatch<SetStateAction<ColorSchemeName>>;
}

const ColorSchemeContext = createContext<ColorSchemeContextProps>({
  colorScheme: null,
  setColorScheme: () => null,
});

const useColorScheme = () => {
  const context = useContext(ColorSchemeContext);
  if (!context) {
    throw new Error(
      "Component has to be inside the Color Scheme Provider to use the hook"
    );
  }
  return context;
};

interface ColorSchemeProps {
  children: ReactNode;
}

const ColorScheme = ({ children }: ColorSchemeProps) => {
  const defaultColorScheme = useRNColorScheme();
  const [colorScheme, setColorScheme] = useState<ColorSchemeName>(
    defaultColorScheme
  );
  const value = { colorScheme, setColorScheme };
  return (
    <ColorSchemeContext.Provider value={value}>
      {children}
    </ColorSchemeContext.Provider>
  );
};

export default ColorScheme;
export { useColorScheme };
