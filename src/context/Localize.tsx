import React, {
  createContext,
  Dispatch,
  ReactNode,
  SetStateAction,
  useContext,
  useMemo,
} from "react";
import * as Localization from "expo-localization";
import i18n from "i18n-js";
import { de, en, fr } from "../translations";

i18n.fallbacks = "en";
i18n.translations = { en, de, fr };

interface LocalizationContextProps {
  t: (scope: i18n.Scope, options?: i18n.TranslateOptions | undefined) => string;
  locale: string;
  setLocale: Dispatch<SetStateAction<string>>;
}

const LocalizationContext = createContext<LocalizationContextProps>({
  t: () => "",
  locale: "",
  setLocale: () => "",
});

export const useLocalisation = () => {
  const context = useContext(LocalizationContext);
  if (!context) {
    throw new Error(
      "Component has to be inside the Localization Provider to use the hook"
    );
  }
  return context;
};

interface LocalizationProps {
  children: ReactNode;
}

const Localize = ({ children }: LocalizationProps) => {
  const [locale, setLocale] = React.useState(Localization.locale);
  const value = useMemo(
    () => ({
      t: (scope: i18n.Scope, options: i18n.TranslateOptions | undefined) =>
        i18n.t(scope, { locale, ...options }),
      locale,
      setLocale,
    }),
    [locale]
  );
  return (
    <LocalizationContext.Provider value={value}>
      {children}
    </LocalizationContext.Provider>
  );
};

export default Localize;
