import * as Contacts from "expo-contacts";
import moment from "moment";
import { Entry } from "../redux/entry/types";

export const sliceArrays = (
  arr1: Contacts.Contact[], // priority
  arr2: string[],
  maxLength: number
) => {
  const itemsInArr1 = arr1?.length || 0;
  const itemsInArr2 = arr2?.length || 0;
  if (itemsInArr1 > maxLength) {
    const slicedArr1: Contacts.Contact[] = arr1?.slice(0, maxLength) || [];
    return {
      slicedArr1,
      slicedArr2: [],
    };
  } else {
    const slicedArr1: Contacts.Contact[] = arr1?.slice(0, itemsInArr1) || [];
    const slicedArr2: string[] =
      itemsInArr2 + itemsInArr1 > maxLength
        ? arr2?.slice(0, maxLength - itemsInArr1)
        : arr2 || [];
    return { slicedArr1, slicedArr2 };
  }
};

export const isInLastDays = (days: number, date: string) => {
  const currentTimestamp = moment().unix();
  const latestTimestamp = (currentTimestamp - 60 * 60 * 24 * days).toString();
  const dateTimestamp = moment(date).format("x");
  return dateTimestamp >= latestTimestamp;
};

export const readableEntries = (entries: Entry[]) => {
  let text = "";
  entries.map((entry) => {
    const location = entry.locationName || "keine Ortangabe";
    const date = entry.date;
    const time = entry.time;
    const r = entry.registeredContacts.map((contact, i) => {
      const name = contact.name;
      const phone =
        contact.phoneNumbers &&
        contact.phoneNumbers.length > 0 &&
        contact.phoneNumbers[0].number;
      return `${i !== 0 ? "\n" : ""}${name} ${phone ? `(${phone})` : ``}`;
    });
    const unr = entry.unregisteredContacts.map(
      (name, i) => `${i !== 0 ? " " : ""}${name}`
    );
    const entryToString = `${date} - ${time}\n${location}\n${r}\n${unr}`;
    text = `${text}\n${entryToString}`;
  });
  return text;
};
