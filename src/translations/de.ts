export default {
  app: {
    name: "Kontakt / Tagebuch",
    link: "https://apps.apple.com/de/app/kontakt-tagebuch/id1537775477",
  },
  actions: {
    delete: "Löschen",
    cancel: "Abbrechen",
    "delete-contact": "Kontakt aus der Liste löschen",
    "create-contact": "Neuer Kontakt - wird auch im Adressbuch gespeichert",
    settings: "Einstellungen",
    "location-settings":
      "Kontakt/Tagebuch hat keinen Zugriff auf deinen Standort.",
  },
  navigation: {
    contact: "kontakt",
    diary: "tagebuch",
    edit: "bearbeiten",
    create: "erstellen",
    inform: "informieren",
    settings: "informationen",
    location: "ort",
  },
  entry: {
    time: "Uhrzeit",
    location: "Ort",
    contacts: "Kontakte",
    map: "Auf Karte suchen",
    "map-pin": "Aktueller Standort",
    input: {
      location: "Ort eintragen",
      contacts: "Kontakt eintragen",
      phone: "Telefonnummer eintragen",
    },
  },
  share: {
    text:
      "Das Kontakt/Tagebuch ist im App Store verfügbar. In kürze auch bei Google Play.",
  },
  inform: {
    officials:
      "Um offizielle Stellen zu informieren, kannst du eine Liste deiner Kontakte per E-Mail versenden.",
    text:
      "Mit diesen Personen hattest du in den letzten 14 Tagen Kontakt. Bitte wähle aus, welche Kontakte du per SMS informieren möchtest.",
    sms: {
      message: "Bleibt sicher.",
    },
    email: {
      button: "Email senden",
      subject: "Kontaktliste",
      alert: {
        title: "Keine Standard Mail-App gefunden",
        description: "Bitte Kontaktliste in Zwischenablage speichern",
      },
    },
    clipboard: {
      button: "In Zwischenablage speichern",
      alert: {
        title: "In Zwischenablage gespeichtert",
      },
    },
  },
  settings: {
    general: {
      text:
        "Das Kontakt / Tagebuch erleichtert dir den Überblick zu behalten, wen du wann und wo getroffen hast. Mit dem Kontakt / Tagebuch kannst du alle deine Kontakte der letzten 14 Tage mit zwei Clicks persönlich informieren. Das geht ganz einfach über eine SMS direkt aus der App heraus.\n\nAlle Daten werden ausschließlich auf deinem Smartphone gespeichert. Mit einem Click auf den E-Mail-Button wird in deinem E-Mail-Programm eine Mail mit allen notwendigen Daten deiner Kontakte erstellt. ",
    },
    how: {
      title: "Wie funktionierts?",
      yours: {
        title: "Deine Informationen",
        text:
          "Die Informationen, wo, wann und mit wem du Kontakt hattest, bleiben auf deinem Smartphone – bis du entscheidest, dass du deine Kontakte informieren willst.\n\nIn der Version 1.0 des Kontakt / Tagebuchs gibst du deinen Ort ein oder wählst ihn aus der Karte und fügst deine Kontakte aus deinem Adressbuch hinzu. Selbstverständlich kannst du auch einen neuen Kontakt anlegen in der App. Dieser ist dann auch in deinem Adressbuch gespeichert.\n\nIn dem kommenden Update wird das Kontakt / Tagebuch automatisiert einen ‚Tagebuch-Eintrag’ mit Datum und Uhrzeit anlegen für jeden Ort, an dem du länger als 10 min verweilst. Diesen automatisiert erstellten ‚Tagebuch-Eintrag’ kannst du mit möglichen Kontakten aus deinem Adressbuch verbinden oder weitere Kontakte eintragen. ",
      },
      request: {
        title: "Eine Bitte...",
        text: "Wir sind überzeugt, dass Gemeinsamkeit rockt.",
      },
    },
    imprint: {
      title: "Impressum",
      responsible: {
        title:
          "Diese App wurde herausgegeben von der Lindgrün GmbH.\nKonzipiert, entwickelt und Verantwortlich:",
        text: `Lindgrün GmbH \nWaldenserstraße 2-4 \n10551 Berlin \nSitz der Gesellschaft: Berlin \nGeschäftsführer: Wolfgang Hanke \nAmtsgericht Berlin Charlottenburg HRB: 82403 B`,
      },
      contact: {
        title: "Kontakt",
        text: "hallo@lindgruen-gmbh.com",
      },
    },
    "data-policy": {
      title: "Datenschutz",
      text:
        "Kurz gesagt: Wir erfassen nichts. Kein Tracker oder Ähnliches wurde von uns in die App integriert. Die Systeme von iOS und Android, die für Download und Bezahlung notwendig sind, erfassen persönliche Daten, sonst könntest du die App nicht downloaden. Wir werden z. B. von den App Stores informiert, wie viele Apps geladen wurden.\n\nWir haben darauf geachtet, dass das Kontakt/Tagebuch sowohl bei Android als auch bei iOS ausschließlich die Systemrechte und Funktionen in Anspruch nimmt, die für eine einwandfreie Nutzung benötigt werden. Darüber hinaus werden KEINE Daten erfasst. Damit ist eine ANONYME Nutzung möglich.",
    },
    "disclaimer-of-liability": {
      title: "Haftungsausschluss",
      text:
        "Diese App soll die Nutzenden dabei unterstützen, sich an die eigenen Kontakte zu erinnern, und diese möglichst einfach erfassen. Die Lindgrün GmbH übernimmt keine Gewährleistung für die Vollständigkeit der erfassten Kontakte, Datum, Uhrzeit und Orte. Link auf die Datenschutzwebseite:",
      link: "https://www.lindgruen-gmbh.com/de/tagebuch/",
    },
    "99-cent": {
      title: "99 Cents?",
      text:
        "Diese App wurde NICHT von Steuern, den Eltern oder Opa und Oma finanziert. Wir sind keine Datenkraken oder verkaufen deine Daten. Wir finanzieren unser Leben durch unsere Arbeit, zahlen Löhne, haben keine unterbezahlten Praktikanten und erbringen Sozialleistungen. Nur so können wir ruhigen Gewissens sagen, dass wir unabhängig sind. Daher kostet die App nach den Probewochen einmalig 99 Cent.",
    },
  },
};
