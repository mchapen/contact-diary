export default {
  app: {
    name: "Contact / Diary",
    link: "https://apps.apple.com/de/app/kontakt-tagebuch/id1537775477",
  },
  actions: {
    delete: "Delete",
    cancel: "Cancel",
    "delete-contact": "Delete contact from list",
    "create-contact": "Create contact - will be saved in your address book",
    settings: "Settings",
    "location-settings": "Contact/Diary does not have access to your location.",
  },
  navigation: {
    contact: "contact",
    diary: "diary",
    edit: "edit",
    create: "create",
    inform: "inform",
    settings: "informations",
    location: "location",
  },
  entry: {
    time: "Time",
    location: "Location",
    contacts: "Contacts",
    map: "Search on map",
    "map-pin": "Current location",
    input: {
      location: "Enter location",
      contacts: "Enter contact",
      phone: "Enter phone number",
    },
  },
  share: {
    text:
      "The Contact/Diary is available in the App Store. Shortly also at Google Play.",
  },
  inform: {
    officials:
      "To inform your respective health authority, send a list of your contacts via e-mail.",
    text:
      "You have been in contact with these people in the last 14 days. Please select which contacts you would like to inform via SMS.",
    sms: {
      message: "Stay save.",
    },
    email: {
      button: "Send mail",
      subject: "Contact list",
      alert: {
        title: "No default Mail-App found",
        description: "Please save the contact list to your clipboard",
      },
    },
    clipboard: {
      button: "Copy to clipboard",
      alert: {
        title: "Saved to Clipboard",
      },
    },
  },
  settings: {
    general: {
      text:
        "The Contact / Diary will make it easier for you to keep on track as to who you ́ve met with when and where. You will be able to personally inform all your contacts of the past fourteen days by sending a text message via the app by two simple clicks. All data is stored exclusively on your smart phone. With just one click on the e-mail button the app will create a mail in your e-mail program containing all the necessary information concerning your contacts.",
    },
    how: {
      title: "How does it work?",
      yours: {
        title: "Your informations",
        text:
          "The information regarding who you met with when and where will remain on your smartphone alone – until you decide that you want to inform your contacts. It is your decision alone who you want to inform when. \n\nIn version 1.0 of the Contact / Diary you simply enter your location or you select it from the map and then you add your contacts from your address book. Of course, you may also create a new contact. This contact is then also immediately stored in your address book.\n\nIn the update soon to come, the Contact / Diary will automatically create a “diary entry” for every location at which you spend more than 10 minutes, containing date, time and location. This automatically created “diary entry” can be linked with possible contacts from your address book and/or you can enter further contacts.",
      },
      request: {
        title: "Something we would like to ask of you...",
        text: "We are convinced that unity rocks.",
      },
    },
    imprint: {
      title: "Imprint",
      responsible: {
        title: "Conception, developement and responsible:",
        text: `Lindgrün GmbH \nWaldenserstraße 2-4 \n10551 Berlin \nSite of the company´s registered office: Berlin \nManaging director: Wolfgang Hanke \District court Berlin Charlottenburg HRB: 82403 B`,
      },
      contact: {
        title: "Contact",
        text: "hallo@lindgruen-gmbh.com",
      },
    },
    "data-policy": {
      title: "Data protection",
      text:
        "To put it briefly, we do not record any data at all. No tracker or the like has been integrated into this app. However, the systems developed by iOS or by Android that are necessary for downloading the app and for payment do collect personal data. Otherwise, you would not be able to download this app. And we, for example, will be informed by app stores as to how often the app has been downloaded. \n\nWe have ensured that, both for iOS and for Android, the Contact / Diary uses exclusively those system rights and functions that are necessary for a flawless utilization of the app. Beyond that, NO data will be recorded. Thus, an entirely ANONYMOUS use of the app is possible.\nOnce you inform your contacts, however, this of course changes. Because then you let your contacts know that you are using this app. ",
    },
    "disclaimer-of-liability": {
      title: "Disclaimer",
      text:
        "This app is meant to support those using it to remind themselves of the personal contacts they have had and to record these in an easy way. Lindgrün GmbH disclaims any warranty regarding the completeness and accuracy of the contacts, dates, times, and locations recorded. Link onto the data protection website:",
      link: "https://www.lindgruen-gmbh.com/de/tagebuch/",
    },
    "99-cent": {
      title: "99 Cents?",
      text:
        "This app was NOT funded through tax, parents or grandparents. We are no data kraken, nor are we intending to sell your data. We pay for our living expenses by working, we pay wages, we do not employ underpaid interns, and we produce social benefits. It is on the basis of this that we can confidently say that we are independent. Therefore, this app will be available for a one-time payment of 99 cents once the cost-free trial month has ended.",
    },
  },
};
