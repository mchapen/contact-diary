export default {
  app: {
    name: "Journal / Contacts",
    link: "https://apps.apple.com/de/app/kontakt-tagebuch/id1537775477",
  },
  actions: {
    delete: "Supprimer",
    cancel: "Annuler",
    "delete-contact": "Supprimer le contact de la liste",
    "create-contact": "Nouveau contact - sera enregister dans ton annuaire",
    settings: "Einstellungen",
    "location-settings": "Journal/Contacts n'a pas accès à votre localisation",
  },
  navigation: {
    contact: "contact",
    diary: "journal",
    edit: "éditer",
    create: "créer",
    inform: "informer",
    settings: "informations",
    location: "lieu",
  },
  entry: {
    time: "Heure",
    location: "Lieu",
    contacts: "Contacts",
    map: "Chercher sur la carte",
    "map-pin": "Lieu actuel",
    input: {
      location: "Saisir le lieu",
      contacts: "Saisir le contact",
      phone: "Saisir le numéro de téléphone",
    },
  },
  share: {
    text:
      "Le Contact/Journal est disponible dans l'App Store. Bientôt aussi sur Google Play.",
  },
  inform: {
    officials:
      "Pour informer le service de santé public, envoye une liste de tes contacts par courrier électronique.",
    text:
      "Ce sont les personnes avec lesquelles vous avez été en contact au cours des 14 derniers jours. Veuillez sélectionner les contacts que vous souhaitez informer.",
    sms: {
      message: "Restez en sécurité.",
    },
    email: {
      button: "Envoyer un courrier électronique",
      subject: "Liste des contacts",
      alert: {
        title: "Aucune application de courrier par défaut trouvée",
        description:
          "Veuillez enregistrer la liste des contacts dans la function copier-coler",
      },
    },
    clipboard: {
      button: "Poser dans la function copier-coler",
      alert: {
        title: "Enregistré dans la function copier-coler",
      },
    },
  },
  settings: {
    general: {
      text:
        "Le Journal / Contacts vous permet de garder plus facilement la trace de qui vous avez rencontré, quand et où. Avec le Journal / Contacts, vous pouvez informer personnellement tous vos contacts des 14 derniers jours en deux clics.\n\nToutes les données sont sauvegardées exclusivement sur votre smartphone. En un clic sur le bouton 'courrier électronique', un message contenant toutes les données nécessaires de vos contacts sera créé dans votre programme de courrier électronique.",
    },
    how: {
      title: "Comment fonctionne l’application ?",
      yours: {
        title: "Tes informations",
        text:
          "Les informations sur les contacts (qui, quand et où) que tu as eus ne sont localisées que sur ton smartphone… jusqu’à ce que tu te décides à les informer. C’est toi qui décides quand tu informes et qui tu informes. Dans la version 1.0 du journal en ligne, tu rentres un lieu en le choisissant sur la carte et tu ajoutes les contacts que tu as eus. Les contacts de ton carnet d’adresses ou bien un nouveau contact. Celui-ci sera alors automatiquement ajouté à ton carnet d’adresse. Dans la prochaine mise à jour de l’application, le journal en ligne créera automatiquement une entrée dans le journal avec date et heure pour chaque lieu dans lequel tu resteras plus de 10 minutes. Tu pourras ensuite ajouter les personnes que tu auras rencontrées.",
      },
      inform: {
        title: "Informer les contacts:",
        text:
          "Tu peux informer tous les contacts par SMS. L’application propose dans ce cas de figure tous les contacts des derniers 14 jours. Nous souhaitons ainsi t’encourager à informer tes contacts, à partir du moment d’une possible infection. Tu pourras envoyer un courriel aux services de santé public ou bien à d’autres administrations.",
      },
      request: {
        title: "S’il vous plaît…",
        text: "Nous sommes convaincus que l'unité est essentielle.",
      },
    },
    imprint: {
      title: "Impressum",
      responsible: {
        title: "Konzipiert, entwickelt und Verantwortlich:",
        text: `Lindgrün GmbH \nWaldenserstraße 2-4 \n10551 Berlin \nSiège de la société: Berlin \nDirecteur général: Wolfgang Hanke \nTribunal d’instance Berlin Charlottenburg HRB: 82403 B`,
      },
      contact: {
        title: "Contact",
        text: "hallo@lindgruen-gmbh.com",
      },
    },
    "data-policy": {
      title: "Protection des données",
      text:
        "Nous ne sauvegardons rien. Aucun traceur n’a été intégré dans l’application. Les systèmes iOS et Android, nécessaires pour le téléchargement, enregistrent des données personnelles, sans lesquelles le téléchargement de l’application n’est pas possible. Nous serons tenus informés par exemple par App Store du nombre de téléchargement de l’application.\n\nNous avons pris soin de veiller à ce que notre journal en ligne, que ce soit pour iOS ou pour Android, n’utilise que les fonctions nécessaires au bon fonctionnement de l’application. Aucunes autres données ne sont enregistrées. Cela assure une utilisation ANONYME du journal. Lorsque tu informes tes contacts, cela change, tu fais savoir à tes contacts que tu utilises l’application.",
    },
    "disclaimer-of-liability": {
      title: "Clause de non-responsabilité",
      text:
        "Cette application est destinée à aider les utilisateurs à sauvegarder les informations concernant leurs déplacements et les contacts qu’ils ont eus. La société Lindgrün GmbH ne garantit pas l’exhaustivité des contacts enregistrés, date, heure et lieu. Site web de confidentialité:",
      link: "https://www.lindgruen-gmbh.com/de/tagebuch/",
    },
    "99-cent": {
      title: "99 centimes?",
      text:
        "Cette application n’a PAS été financée par les impôts, les parents ou grands-parents. Nous ne vendons pas tes données. C’est notre travail qui nous finance, nous payons des salaires, nous n’avons pas de stagiaires sous-payés, nous payons nos cotisations sociales. C’est ainsi que nous prouvons notre indépendance. Par conséquent, l’application a un coût de 99 cent lors du téléchargement, après les semaines d’essai.",
    },
  },
};
