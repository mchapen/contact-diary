export { default as de } from "./de";
export { default as en } from "./en";
export { default as fr } from "./fr";
export { default as calendarDE } from "./de-calendar.json";
export { default as calendarEN } from "./en-calendar.json";
export { default as calendarFR } from "./fr-calendar.json";
