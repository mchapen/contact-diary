import React from "react";
import ActionButton from "./ActionButton";
import Animated, { Extrapolate } from "react-native-reanimated";
import theme from "../Theme";
import { useDispatch } from "react-redux";
import { setColorScheme } from "../../redux/app/actions";

interface ThemeGroupProps {
  getTopPosition: (radiant: number, buttonSize: number) => number;
  getLeftPosition: (radiant: number, buttonSize: number) => number;
  size: number;
  animatedValue: Animated.Value<number>;
}

const ThemeGroup = ({
  getLeftPosition,
  getTopPosition,
  size,
  animatedValue,
}: ThemeGroupProps) => {
  const inputRange = [1, 1.5, 2];
  const opacity = animatedValue.interpolate({
    inputRange,
    outputRange: [0, 0.5, 1],
  });
  const animatedSize = animatedValue.interpolate({
    inputRange,
    outputRange: [0, size / 2, size],
    extrapolate: Extrapolate.IDENTITY,
  });
  const dispatch = useDispatch();
  return (
    <>
      <ActionButton
        onPress={() => dispatch(setColorScheme("seaSide"))}
        top={getTopPosition((Math.PI * 3.5) / 6, size)}
        left={getLeftPosition((Math.PI * 3.5) / 6, size)} // -
        {...{ size, opacity, animatedSize }}
        colors={{
          primary: theme.colors.seaSidePrimary,
          secondary: theme.colors.seaSideSecondary,
        }}
      />
      <ActionButton
        onPress={() => dispatch(setColorScheme("technoParty"))}
        top={getTopPosition((Math.PI * 4.5) / 6, size)}
        left={getLeftPosition((Math.PI * 4.5) / 6, size)} // -
        {...{ size, opacity, animatedSize }}
        colors={{
          primary: theme.colors.technoPartyPrimary,
          secondary: theme.colors.technoPartySecondary,
        }}
      />
      <ActionButton
        onPress={() => dispatch(setColorScheme("hyggeHome"))}
        top={getTopPosition((Math.PI * 5.5) / 6, size)}
        left={getLeftPosition((Math.PI * 5.5) / 6, size)} // -
        {...{ size, opacity, animatedSize }}
        colors={{
          primary: theme.colors.hyggeHomePrimary,
          secondary: theme.colors.hyggeHomeSecondary,
        }}
      />
      <ActionButton
        onPress={() => dispatch(setColorScheme("violetDream"))}
        top={getTopPosition((Math.PI * 6.5) / 6, size)}
        left={getLeftPosition((Math.PI * 6.5) / 6, size)} // -
        {...{ size, opacity, animatedSize }}
        colors={{
          primary: theme.colors.violetDreamPrimary,
          secondary: theme.colors.violetDreamSecondary,
        }}
      />
      <ActionButton
        onPress={() => dispatch(setColorScheme("friendlyRose"))}
        top={getTopPosition((Math.PI * 7.5) / 6, size)}
        left={getLeftPosition((Math.PI * 7.5) / 6, size)} // -
        {...{ size, opacity, animatedSize }}
        colors={{
          primary: theme.colors.friendlyRosePrimary,
          secondary: theme.colors.friendlyRoseSecondary,
        }}
      />
      <ActionButton
        onPress={() => dispatch(setColorScheme("nightStorm"))}
        top={getTopPosition((Math.PI * 8.5) / 6, size)}
        left={getLeftPosition((Math.PI * 8.5) / 6, size)} // -
        {...{ size, opacity, animatedSize }}
        colors={{
          primary: theme.colors.nightStormPrimary,
          secondary: theme.colors.nightStormSecondary,
        }}
      />
    </>
  );
};

export default ThemeGroup;
