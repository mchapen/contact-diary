import { useNavigation } from "@react-navigation/native";
import React, { Dispatch, SetStateAction } from "react";
import { View } from "react-native";
import Animated, { Extrapolate } from "react-native-reanimated";
import { useTheme } from "../Theme";
import ActionButton from "./ActionButton";

interface MainGroupProps {
  getTopPosition: (radiant: number, buttonSize: number) => number;
  getLeftPosition: (radiant: number, buttonSize: number) => number;
  setValue: Dispatch<SetStateAction<number>>;
  size: number;
  animatedValue: Animated.Value<number>;
}

const MainGroup = ({
  getLeftPosition,
  getTopPosition,
  setValue,
  size,
  animatedValue,
}: MainGroupProps) => {
  const navigation = useNavigation();
  const inputRange = [0, 0.5, 1, 1.5, 2];
  const opacity = animatedValue.interpolate({
    inputRange,
    outputRange: [0, 0.5, 1, 0.5, 0],
  });
  const animatedSize = animatedValue.interpolate({
    inputRange: [0, 0.5, 1, 1.5, 2],
    outputRange: [0, size / 2, size, size, size],
    extrapolate: Extrapolate.IDENTITY,
  });
  const animatedIconSize = animatedValue.interpolate({
    inputRange: [0, 0.5, 1, 1.5, 2],
    outputRange: [0, size / 4, size / 2, size / 2, size / 2],
    extrapolate: Extrapolate.IDENTITY,
  });
  const theme = useTheme();
  return (
    <>
      <ActionButton
        onPress={() => setValue(2)}
        top={getTopPosition(1, size)}
        left={getLeftPosition(1, size)}
        icon="slash"
        colors={{
          primary: theme.colors.primary,
          secondary: theme.colors.secondary,
        }}
        {...{ size, opacity, animatedSize, animatedIconSize }}
      />
      <ActionButton
        onPress={() => navigation.navigate("Edit")}
        top={getTopPosition(0, size)}
        left={getLeftPosition(0, size)}
        icon="plus"
        {...{ size, opacity, animatedSize, animatedIconSize }}
      />
      <ActionButton
        onPress={() => navigation.navigate("Inform")}
        top={getTopPosition(-1, size)}
        left={getLeftPosition(-1, size)}
        icon="send"
        {...{ size, opacity, animatedSize, animatedIconSize }}
      />
    </>
  );
};

export default MainGroup;
