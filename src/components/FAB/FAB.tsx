import React, { useEffect, useRef, useState } from "react";
import { useWindowDimensions } from "react-native";
import Animated, { Easing, Extrapolate } from "react-native-reanimated";
import { Box, useTheme } from "../Theme";
import { Feather as Icon } from "@expo/vector-icons";
import BorderlessTap from "../BorderlessTap";
import MainGroup from "./MainGroup";
import ThemeGroup from "./ThemeGroup";

const size = 300;
const mainButtonSize = 75;
const actionButtonSize = 60;
const themeButtonSize = 40;
const buttonSpacing = 10;
const defaultRotation = Math.PI;
const radius = mainButtonSize / 2 + 2 * buttonSpacing + actionButtonSize / 2;

const AnimatedBox = Animated.createAnimatedComponent(Box);

interface FABProps {}
const FAB = ({}: FABProps) => {
  const [value, setValue] = useState(0);
  const animatedValue = useRef(new Animated.Value(0)).current;
  const { width } = useWindowDimensions();
  const inputRange = [0, 1, 2];
  const rotate = animatedValue.interpolate({
    inputRange,
    outputRange: [
      0 - defaultRotation,
      Math.PI - defaultRotation,
      Math.PI * 2 - defaultRotation,
    ],
    extrapolate: Extrapolate.CLAMP,
  });
  const theme = useTheme();

  useEffect(() => {
    animation(value).start();
  }, [value]);

  const animation = (toValue: number) =>
    Animated.timing(animatedValue, {
      duration: 500,
      toValue,
      easing: Easing.in(Easing.linear),
    });

  const getTopPosition = (radiant: number, buttonSize: number) =>
    mainButtonSize / 2 - buttonSize / 2 - Math.cos(radiant) * radius;
  const getLeftPosition = (radiant: number, buttonSize: number) =>
    mainButtonSize / 2 - buttonSize / 2 - Math.sin(radiant) * radius;

  return (
    <Box
      position="absolute"
      bottom={theme.spacing.m}
      alignItems="center"
      left={(width - mainButtonSize) / 2}
      right={(width - mainButtonSize) / 2}
    >
      <AnimatedBox
        height={mainButtonSize}
        width={mainButtonSize}
        borderWidth={1}
        borderColor="secondary"
        borderRadius={mainButtonSize / 2}
        justifyContent="center"
        alignItems="center"
        style={{ transform: [{ rotate }] }}
      >
        <BorderlessTap onPress={() => setValue((prev) => (prev > 0 ? 0 : 1))}>
          <Box
            backgroundColor="secondary"
            height={mainButtonSize}
            width={mainButtonSize}
            borderRadius={mainButtonSize / 2}
            alignItems="center"
            justifyContent="center"
          >
            <Icon name="rotate-cw" size={40} color={"white"} />
          </Box>
        </BorderlessTap>
        <MainGroup
          getLeftPosition={getLeftPosition}
          getTopPosition={getTopPosition}
          setValue={setValue}
          size={actionButtonSize}
          animatedValue={animatedValue}
        />
        <ThemeGroup
          getLeftPosition={getLeftPosition}
          getTopPosition={getTopPosition}
          size={themeButtonSize}
          animatedValue={animatedValue}
        />
      </AnimatedBox>
    </Box>
  );
};

export default FAB;
