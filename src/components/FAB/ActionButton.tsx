import React, { useEffect, useRef, useState } from "react";
import Animated from "react-native-reanimated";
import { Box, Theme, useTheme } from "../Theme";
import { Feather as Icon } from "@expo/vector-icons";
import BorderlessTap from "../BorderlessTap";
import { BoxProps } from "@shopify/restyle";
import { StyleSheet } from "react-native";

const AnimatedBox = Animated.createAnimatedComponent(Box);
const AnimatedIcon = Animated.createAnimatedComponent(Icon);

interface ActionButtonProps {
  onPress: () => void;
  animatedValue?: number;
  icon?: string;
  size: number;
  top: number;
  left: number;
  opacity?: Animated.Adaptable<number>;
  animatedSize?: Animated.Adaptable<number>;
  animatedIconSize?: Animated.Adaptable<number>;
  variant: "default" | "primary";
  colors?: {
    primary: string;
    secondary: string;
  };
}

const ActionButton = ({
  onPress,
  icon,
  size,
  variant,
  opacity,
  colors,
  animatedSize,
  animatedIconSize,
  ...props
}: ActionButtonProps) => {
  const theme = useTheme();
  return (
    <AnimatedBox
      position="absolute"
      alignItems="center"
      justifyContent="center"
      opacity={opacity}
      height={animatedSize}
      width={animatedSize}
      borderRadius={size / 2}
      overflow="hidden"
      {...props}
    >
      <BorderlessTap {...{ onPress }}>
        <Box alignItems="center" justifyContent="center">
          {colors ? (
            <Box flex={1} style={{ transform: [{ rotate: "45deg" }] }}>
              <Box
                height={size / 2}
                width={size}
                style={{ backgroundColor: colors.primary }}
              />
              <Box
                height={size / 2}
                width={size}
                style={{ backgroundColor: colors.secondary }}
              />
            </Box>
          ) : (
            <Box height={size} width={size} backgroundColor="secondary" />
          )}
          {icon && (
            <Box position="absolute" zIndex={1}>
              <AnimatedIcon
                name={icon}
                color={"white"}
                style={{
                  fontSize: animatedIconSize || size / 2,
                }}
              />
            </Box>
          )}
        </Box>
      </BorderlessTap>
    </AnimatedBox>
  );
};

ActionButton.defaultProps = {
  variant: "default",
};

export default ActionButton;
