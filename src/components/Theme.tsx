import React, { ReactNode } from "react";
import {
  createTheme,
  createBox,
  createText,
  useTheme as useReTheme,
  ThemeProvider as ReThemeProvider,
  createRestyleComponent,
  VariantProps,
  createVariant,
} from "@shopify/restyle";
import { StatusBar } from "expo-status-bar";
import { useSelector } from "../redux";
import { useColorScheme } from "react-native";

const palette = {
  white: "#fdfffc",
  black: "#011627",
  lightgrey: "#edf2f4",
  darkgrey: "#344552",

  primary: "#e71d36",
  secondary: "#2ec4b6",

  success: "#328e32",
  danger: "#e21617",

  // hyggehome
  hyggeHomePrimary: "#382F2F",
  hyggeHomePrimaryLight: "#AFA7A7",
  hyggeHomeSecondary: "#ED7200",
  hyggeHomeCard: "#F0EDED",
  hyggeHomeForeground: "#382F2F",

  // friendlyRose
  friendlyRosePrimary: "#FD8777",
  friendlyRosePrimaryLight: "#F5D1CB",
  friendlyRoseSecondary: "#FF7E67",
  friendlyRoseCard: "#FCE6E2",
  friendlyRoseForeground: "#382F2F",

  // violetDream
  violetDreamPrimary: "#A71680",
  violetDreamPrimaryLight: "#D599C5",
  violetDreamSecondary: "#841065",
  violetDreamCard: "#EFE1EB",
  violetDreamForeground: "#382F2F",

  // technoParty
  technoPartyPrimary: "#050505",
  technoPartyPrimaryLight: "#050505",
  technoPartySecondary: "#A71680",
  technoPartyCard: "#38072A",
  technoPartyForeground: "#FFFFFF",

  // nightStorm
  nightStormPrimary: "#081526",
  nightStormPrimaryLight: "#0C213C",
  nightStormSecondary: "#78C7C1",
  nightStormCard: "#254062",
  nightStormForeground: "#F8F2F2",

  // seaSide
  seaSidePrimary: "#005B71",
  seaSidePrimaryLight: "#DCEDED",
  seaSideSecondary: "#ED4E08",
  seaSideCard: "#FFFFFF",
  seaSideForeground: "#005B71",
};

const theme = createTheme({
  colors: {
    white: palette.white,
    mainBackground: palette.white,
    mainForeground: palette.black,
    navigationForeground: palette.black,
    primary: palette.primary,
    secondary: palette.secondary,
    danger: palette.danger,
    success: palette.success,
    cardPrimary: palette.lightgrey,
    inputFocus: palette.primary,
    // inputDanger
    calendarPrimary: palette.secondary,

    // colorSchemeColors
    hyggeHomePrimary: palette.hyggeHomePrimary,
    hyggeHomeSecondary: palette.hyggeHomeSecondary,
    violetDreamPrimary: palette.violetDreamPrimary,
    violetDreamSecondary: palette.violetDreamSecondary,
    friendlyRosePrimary: palette.friendlyRosePrimary,
    friendlyRoseSecondary: palette.friendlyRoseSecondary,
    technoPartyPrimary: palette.technoPartyPrimary,
    technoPartySecondary: palette.technoPartySecondary,
    nightStormPrimary: palette.nightStormPrimary,
    nightStormSecondary: palette.nightStormSecondary,
    seaSidePrimary: palette.seaSidePrimary,
    seaSideSecondary: palette.seaSideSecondary,
  },
  spacing: {
    xs: 4,
    s: 8,
    m: 16,
    l: 24,
    xl: 40,
  },
  textVariants: {
    navigationTitle: {
      fontSize: 22,
      fontFamily: "Disket-Mono-Bold",
      lineHeight: 26,
    },
    hero: {
      fontSize: 60,
      fontFamily: "RobotoSlab-SemiBold",
      lineHeight: 90,
    },
    title: {
      fontSize: 24,
      fontFamily: "RobotoSlab-SemiBold",
      lineHeight: 26,
    },
    subtitle: {
      fontSize: 22,
      fontFamily: "RobotoSlab-Regular",
      lineHeight: 26,
    },
    body: {
      fontSize: 18,
      fontFamily: "RobotoSlab-Regular",
      lineHeight: 24,
    },
    description: {
      fontSize: 15,
      fontFamily: "RobotoSlab-Regular",
      lineHeight: 18,
    },
    button: {
      fontSize: 20,
      fontFamily: "RobotoSlab-Regular",
      lineHeight: 26,
    },
  },
  breakpoints: {
    phone: 0,
    iphone8: 375,
    tablet: 768,
  },
  cardVariants: {
    primary: {
      backgroundColor: "cardPrimary",
      padding: "m",
      borderRadius: 8, // spacing.s === 8
      marginBottom: "s",
      shadowColor: "mainForeground",
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.3,
      shadowRadius: 3,
      elevation: 8,
    },
  },
});

export type Theme = typeof theme;

const hyggeHomeTheme: Theme = {
  ...theme,
  colors: {
    ...theme.colors,
    mainForeground: palette.hyggeHomeForeground,
    mainBackground: palette.hyggeHomePrimaryLight,
    navigationForeground: palette.hyggeHomeCard,
    primary: palette.hyggeHomePrimary,
    secondary: palette.hyggeHomeSecondary,
    calendarPrimary: palette.hyggeHomeSecondary,
    cardPrimary: palette.hyggeHomeCard,
    inputFocus: palette.hyggeHomePrimary,
  },
};

const friendlyRoseTheme: Theme = {
  ...theme,
  colors: {
    ...theme.colors,
    mainForeground: palette.friendlyRoseForeground,
    mainBackground: palette.friendlyRosePrimaryLight,
    navigationForeground: palette.friendlyRoseForeground,
    primary: palette.friendlyRosePrimary,
    secondary: palette.friendlyRoseSecondary,
    calendarPrimary: palette.friendlyRoseSecondary,
    cardPrimary: palette.friendlyRoseCard,
    inputFocus: palette.friendlyRosePrimary,
  },
};

const violetDreamTheme: Theme = {
  ...theme,
  colors: {
    ...theme.colors,
    mainForeground: palette.violetDreamForeground,
    mainBackground: palette.violetDreamPrimaryLight,
    navigationForeground: palette.violetDreamCard,
    primary: palette.violetDreamPrimary,
    secondary: palette.violetDreamSecondary,
    calendarPrimary: palette.violetDreamSecondary,
    cardPrimary: palette.violetDreamCard,
    inputFocus: palette.violetDreamPrimary,
  },
};

const technoPartyTheme: Theme = {
  ...theme,
  colors: {
    ...theme.colors,
    mainForeground: palette.technoPartyForeground,
    mainBackground: palette.technoPartyPrimaryLight,
    navigationForeground: palette.technoPartyForeground,
    primary: palette.technoPartyPrimary,
    secondary: palette.technoPartySecondary,
    calendarPrimary: palette.technoPartySecondary,
    cardPrimary: palette.technoPartyCard,
    inputFocus: palette.technoPartyPrimary,
  },
};

const nightStormTheme: Theme = {
  ...theme,
  colors: {
    ...theme.colors,
    mainForeground: palette.nightStormForeground,
    mainBackground: palette.nightStormPrimaryLight,
    navigationForeground: palette.nightStormForeground,
    primary: palette.nightStormPrimary,
    secondary: palette.nightStormSecondary,
    calendarPrimary: palette.nightStormSecondary,
    cardPrimary: palette.nightStormCard,
    inputFocus: palette.nightStormPrimary,
  },
};

const seaSideTheme: Theme = {
  ...theme,
  colors: {
    ...theme.colors,
    mainForeground: palette.seaSideForeground,
    mainBackground: palette.seaSidePrimaryLight,
    navigationForeground: palette.seaSideCard,
    primary: palette.seaSidePrimary,
    secondary: palette.seaSideSecondary,
    calendarPrimary: palette.seaSideSecondary,
    cardPrimary: palette.seaSideCard,
    inputFocus: palette.seaSidePrimary,
  },
};

interface ThemeProviderProps {
  children: ReactNode;
}

export const ThemeProvider = ({ children }: ThemeProviderProps) => {
  const defaultColorScheme = useColorScheme();
  const colorScheme = useSelector((state) => state.app.colorScheme);
  const currentTheme = colorScheme
    ? colorScheme === "friendlyRose"
      ? friendlyRoseTheme
      : colorScheme === "hyggeHome"
      ? hyggeHomeTheme
      : colorScheme === "violetDream"
      ? violetDreamTheme
      : colorScheme === "technoParty"
      ? technoPartyTheme
      : colorScheme === "nightStorm"
      ? nightStormTheme
      : colorScheme === "seaSide"
      ? seaSideTheme
      : nightStormTheme
    : defaultColorScheme === "dark"
    ? nightStormTheme
    : seaSideTheme;
  const statusBarTheme = colorScheme === "friendlyRose" ? "dark" : "light";
  return (
    <ReThemeProvider theme={currentTheme}>
      <StatusBar style={statusBarTheme} />
      {children}
    </ReThemeProvider>
  );
};

export const Box = createBox<Theme>();
export const Text = createText<Theme>();
export const useTheme = () => useReTheme<Theme>();

export const Card = createRestyleComponent<
  VariantProps<Theme, "cardVariants"> & React.ComponentProps<typeof Box>,
  Theme
>([createVariant({ themeKey: "cardVariants" })], Box);

Text.defaultProps = {
  variant: "body",
  color: "mainForeground",
};

Box.defaultProps = {};

Card.defaultProps = {
  variant: "primary",
};

export default theme;
