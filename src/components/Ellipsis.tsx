import React, { useEffect, useRef, useState } from "react";
import { Box } from "./Theme";
import Animated, { Easing } from "react-native-reanimated";

const numberOfDots = 3;
const size = 6;
const AnimatedBox = Animated.createAnimatedComponent(Box);

interface EllipsisProps {}

const Ellipsis = ({}: EllipsisProps) => {
  const animatedValue = useRef(new Animated.Value(0)).current;
  const [flag, setFlag] = useState(0);
  const arr = new Array(numberOfDots).fill(0);
  const animation = (toValue: number) =>
    Animated.timing(animatedValue, {
      toValue,
      duration: 1000,
      easing: Easing.in(Easing.linear),
    });
  useEffect(() => {
    animation(flag === 0 ? numberOfDots : 0).start(() =>
      setFlag((prev) => (prev === 1 ? 0 : 1))
    );
  }, [flag]);
  return (
    <Box flexDirection="row" alignItems="center" height={18} width={50}>
      {arr.map((_, index) => {
        const opacity = animatedValue.interpolate({
          inputRange: [index, index + 1],
          outputRange: [0, 1],
        });
        return (
          <AnimatedBox
            key={index}
            height={size}
            width={size}
            borderRadius={size / 2}
            backgroundColor="secondary"
            marginRight="xs"
            style={{ opacity }}
          />
        );
      })}
    </Box>
  );
};

export default Ellipsis;
