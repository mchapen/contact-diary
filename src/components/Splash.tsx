import React, { Dispatch, SetStateAction, useEffect, useRef } from "react";
import { Image, ImageBackground, useWindowDimensions } from "react-native";
import Animated, { Easing } from "react-native-reanimated";
import { useLocalisation } from "../context";
import { Box, Text } from "./Theme";

const spashImage = require("../../assets/splash.png");
const AnimatedText = Animated.createAnimatedComponent(Text);

type Direction = "left" | "right";

interface SpashProps {
  onCallback: () => void;
}
const Spash = ({ onCallback }: SpashProps) => {
  const { t } = useLocalisation();
  const { width, height } = useWindowDimensions();
  const animatedValue = useRef(new Animated.Value(0)).current;
  const animation = () =>
    Animated.timing(animatedValue, {
      toValue: 1,
      duration: 1000,
      easing: Easing.bezier(1, 0.25, 0.25, 1),
    });
  useEffect(() => {
    animation().start(() => onCallback());
  }, []);
  const translateX = (direction: Direction) =>
    animatedValue.interpolate({
      inputRange: [0, 1],
      outputRange: [(width / 2) * (direction === "left" ? -1 : 1), 0],
    });
  return (
    <ImageBackground source={spashImage} style={{ flex: 1 }}>
      <Box
        position="absolute"
        left={0}
        right={0}
        bottom={height / 5}
        flexDirection="row"
        justifyContent="center"
      >
        <AnimatedText
          variant="navigationTitle"
          color="white"
          style={{ transform: [{ translateX: translateX("left") }] }}
        >
          {t("navigation.contact").toUpperCase()}/
        </AnimatedText>
        <AnimatedText
          variant="navigationTitle"
          fontFamily="Disket-Mono-Regular"
          color="white"
          style={{ transform: [{ translateX: translateX("right") }] }}
        >
          {t("navigation.diary").toUpperCase()}
        </AnimatedText>
      </Box>
    </ImageBackground>
  );
};

export default Spash;
