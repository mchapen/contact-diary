import React from "react";
import { View } from "react-native";
import { Box, Text, Theme, useTheme } from "./Theme";
import { Feather as Icon } from "@expo/vector-icons";
import { BorderlessButton } from "react-native-gesture-handler";
import { BoxProps } from "@shopify/restyle";

interface ButtonWithIconProps extends BoxProps<Theme> {
  name: string;
  label: string;
  onPress: () => void;
  variant: "default" | "primary";
}
const ButtonWithIcon = ({
  name,
  label,
  onPress,
  variant,
  ...props
}: ButtonWithIconProps) => {
  const theme = useTheme();
  const color =
    variant === "default"
      ? theme.colors.mainForeground
      : theme.colors.secondary;
  return (
    <BorderlessButton onPress={onPress}>
      <Box flexDirection="row" alignItems="center" {...props}>
        <Icon name={name} color={color} size={16} />
        <Text paddingLeft="xs" style={{ color }}>
          {label}
        </Text>
      </Box>
    </BorderlessButton>
  );
};

ButtonWithIcon.defaultProps = {
  variant: "primary",
};

export default ButtonWithIcon;
