import React from "react";
import { useWindowDimensions, View } from "react-native";
import BorderlessTap from "./BorderlessTap";
import { Feather as Icon } from "@expo/vector-icons";
import { Box, useTheme } from "./Theme";

interface MainButtonProps {
  onPress: () => void;
  icon: string;
  size: number;
}
const MainButton = ({ onPress, icon, size }: MainButtonProps) => {
  const theme = useTheme();
  const { width } = useWindowDimensions();
  return (
    <Box
      position="absolute"
      bottom={theme.spacing.m}
      left={(width - size) / 2}
      right={(width - size) / 2}
      alignItems="center"
    >
      <BorderlessTap onPress={onPress}>
        <Box
          height={size}
          width={size}
          borderRadius={size / 2}
          backgroundColor="secondary"
          justifyContent="center"
          alignItems="center"
        >
          <Icon name={icon} size={40} color={"white"} />
        </Box>
      </BorderlessTap>
    </Box>
  );
};

MainButton.defaultProps = {
  size: 75,
};

export default MainButton;
