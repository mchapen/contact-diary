import { RouteProp } from "@react-navigation/native";
import { StackNavigationProp } from "@react-navigation/stack";
import { Dispatch, SetStateAction } from "react";
import { LatLng } from "react-native-maps";
import { Entry } from "../redux/entry/types";

export interface AppNavigationProps<RouteName extends keyof AppRoutes> {
  navigation: StackNavigationProp<AppRoutes, RouteName>;
  route: RouteProp<AppRoutes, RouteName>;
}

export type AppRoutes = {
  Home: undefined;
  Edit: undefined | { item: Entry };
  Settings: undefined;
  Map: { location: LatLng; setLocation: Dispatch<SetStateAction<LatLng>> };
  Inform: undefined;
};
