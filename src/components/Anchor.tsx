import React, { ReactNode } from "react";
import { Text, Theme } from "./Theme";
import * as Linking from "expo-linking";
import { TextProps } from "@shopify/restyle";

interface AnchorProps extends TextProps<Theme> {
  href: string;
  children: ReactNode;
}

const Anchor = ({ href, children, ...props }: AnchorProps) => {
  const handlePress = () => {
    Linking.openURL(href);
  };

  return (
    <Text color="secondary" onPress={handlePress} {...props}>
      {children}
    </Text>
  );
};

export default Anchor;
