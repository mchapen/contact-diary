import React from "react";
import { View } from "react-native";
import { Box, Text, Theme, useTheme } from "./Theme";
import { Feather as Icon } from "@expo/vector-icons";
import * as Contacts from "expo-contacts";
import { BoxProps } from "@shopify/restyle";

export interface ContactProps extends BoxProps<Theme> {
  unregistered?: string;
  registered?: Contacts.Contact;
}

const Contact = ({ unregistered, registered, ...props }: ContactProps) => {
  const theme = useTheme();
  return (
    <Box flexDirection="row" alignItems="center" {...props}>
      <Icon
        name={unregistered ? "user" : "user-check"}
        size={20}
        color={unregistered ? theme.colors.danger : theme.colors.success}
        style={{ paddingLeft: registered ? 2 : 0 }}
      />
      <Text
        numberOfLines={1}
        style={{
          paddingLeft: registered ? theme.spacing.s - 2 : theme.spacing.s,
        }}
      >
        {unregistered || registered?.name}
      </Text>
    </Box>
  );
};

export default Contact;
