import React from "react";
import { Feather as Icon } from "@expo/vector-icons";

import { Theme, Box, Text, useTheme } from "./Theme";

export interface RoundedIconProps {
  name: string;
  size: number;
  color: keyof Theme["colors"];
  backgroundColor: keyof Theme["colors"] | undefined;
  iconRatio: number;
  align: "center" | "flex-start" | "flex-end";
}

const RoundedIcon = ({
  name,
  size,
  color,
  backgroundColor,
  iconRatio,
  align,
}: RoundedIconProps) => {
  const iconSize = size * iconRatio;
  const theme = useTheme();
  return (
    <Box
      height={size}
      width={size}
      justifyContent="center"
      alignItems={align}
      style={{ borderRadius: size / 2 }}
      {...{ backgroundColor }}
    >
      <Icon size={iconSize} {...{ name }} color={theme.colors[color]} />
    </Box>
  );
};

RoundedIcon.defaultProps = {
  iconRatio: 0.7,
  align: "center",
};

export default RoundedIcon;
