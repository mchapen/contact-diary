import React, { useState } from "react";
import {
  TextInput as RNTextInput,
  TextInputProps as RNTextInputProps,
} from "react-native";
import { useTheme } from "../Theme";

interface TextInputProps extends RNTextInputProps {}

const TextInput = ({ style, ...props }: TextInputProps) => {
  const [isFocused, setIsFocused] = useState(false);
  const theme = useTheme();
  return (
    <RNTextInput
      onFocus={() => setIsFocused(true)}
      onBlur={() => setIsFocused(false)}
      placeholderTextColor={theme.colors.mainForeground}
      {...props}
      style={[
        {
          padding: theme.spacing.s,
          borderRadius: theme.spacing.s,
          borderWidth: 1,
          borderColor: isFocused
            ? theme.colors.inputFocus
            : theme.colors.mainForeground,
          backgroundColor: theme.colors.cardPrimary,
          color: theme.colors.mainForeground,
          fontFamily: "RobotoSlab-Light",
        },
        style,
      ]}
    />
  );
};

export default TextInput;
