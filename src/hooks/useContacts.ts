import React, { useEffect, useState } from "react";
import { Platform } from "react-native";
import * as Contacts from "expo-contacts";

const useContacts = (contactName: string) => {
  const [contacts, setContacts] = useState<Contacts.Contact[]>();
  useEffect(() => {
    if (contactName.trim() !== "") {
      (async () => {
        const { status } = await Contacts.requestPermissionsAsync();
        if (status === "granted") {
          if (Platform.OS === "ios") {
            const { data } = await Contacts.getContactsAsync({
              fields: [Contacts.Fields.PhoneNumbers],
              name: contactName,
              pageSize: 5,
            });
            setContacts(data.length > 0 ? data : undefined);
          } else {
            const { data } = await Contacts.getContactsAsync({
              fields: [Contacts.Fields.PhoneNumbers],
            });
            const reg = RegExp(contactName, "gi");
            const filteredData = data.filter((contact) =>
              contact.name.match(reg)
            );
            const slicedData = filteredData.slice(0, 5);
            setContacts(slicedData.length > 0 ? slicedData : undefined);
          }
        }
      })();
    } else {
      setContacts(undefined);
    }
  }, [contactName]);
  return contacts;
};

export default useContacts;
