import {
  UPDATE_ENTRY,
  ADD_ENTRY,
  DELETE_ENTRY,
  EntryActionTypes,
  EntryState,
} from "./types";

const initialState: EntryState = {
  list: [],
};

export function entryReducer(state = initialState, action: EntryActionTypes) {
  switch (action.type) {
    case UPDATE_ENTRY:
      return {
        ...state,
        list: state.list.map((i) =>
          i.id === action.payload.id ? action.payload : i
        ),
      };
    case ADD_ENTRY:
      return {
        ...state,
        list: [
          // removing possible dublicate if clicked multiple times.
          ...state.list.filter((i) => i.id !== action.payload.id),
          action.payload,
        ],
      };
    case DELETE_ENTRY:
      return {
        ...state,
        list: state.list.filter((i) => i.id !== action.payload.id),
      };
    default:
      return state;
  }
}
