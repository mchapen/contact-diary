import * as Contacts from "expo-contacts";
import { LatLng } from "react-native-maps";

export interface Entry {
  id: string;
  timestamp: number;
  time: string;
  date: string;
  unregisteredContacts: string[];
  registeredContacts: Contacts.Contact[];
  locationName?: string;
  location?: LatLng;
}

export interface EntryState {
  list: Entry[];
}

export const UPDATE_ENTRY = "UPDATE_ENTRY";
export const DELETE_ENTRY = "DELETE_ENTRY";
export const ADD_ENTRY = "CREATE_ENTRY";

interface UpdateEntryAction {
  type: typeof UPDATE_ENTRY;
  payload: Entry;
}

interface AddEntryAction {
  type: typeof ADD_ENTRY;
  payload: Entry;
}

interface DeleteEntryAction {
  type: typeof DELETE_ENTRY;
  payload: Entry;
}

export type EntryActionTypes =
  | UpdateEntryAction
  | AddEntryAction
  | DeleteEntryAction;
