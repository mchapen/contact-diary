import {
  EntryActionTypes,
  ADD_ENTRY,
  UPDATE_ENTRY,
  DELETE_ENTRY,
  Entry,
} from "./types";

export function addEntry(entry: Entry): EntryActionTypes {
  return { type: ADD_ENTRY, payload: entry };
}

export function updateEntry(entry: Entry): EntryActionTypes {
  return { type: UPDATE_ENTRY, payload: entry };
}

export function deleteEntry(entry: Entry): EntryActionTypes {
  return { type: DELETE_ENTRY, payload: entry };
}
