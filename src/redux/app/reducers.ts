import {
  AppState,
  SET_CALENDAR_DATE,
  AppActionTypes,
  SET_COLOR_SCHEME,
} from "./types";

const initialState: AppState = {
  colorScheme: undefined,
  selectedDate: "",
};

export function appReducer(state = initialState, action: AppActionTypes) {
  switch (action.type) {
    case SET_CALENDAR_DATE:
      return {
        ...state,
        selectedDate: action.payload,
      };
    case SET_COLOR_SCHEME:
      return {
        ...state,
        colorScheme: action.payload,
      };
    default:
      return state;
  }
}
