import {
  AppActionTypes,
  SET_CALENDAR_DATE,
  SET_COLOR_SCHEME,
  ColorSchemeName,
} from "./types";

export function setCalendarDate(date: string): AppActionTypes {
  return { type: SET_CALENDAR_DATE, payload: date };
}

export function setColorScheme(colorScheme: ColorSchemeName): AppActionTypes {
  return { type: SET_COLOR_SCHEME, payload: colorScheme };
}
