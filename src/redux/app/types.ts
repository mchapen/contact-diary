import * as Contacts from "expo-contacts";
import { LatLng } from "react-native-maps";
import { ColorSchemeName as RNColorSchemeName } from "react-native";

export type ColorSchemeName =
  | RNColorSchemeName
  | "hyggeHome"
  | "friendlyRose"
  | "violetDream"
  | "technoParty"
  | "nightStorm"
  | "seaSide";

export interface AppState {
  colorScheme: ColorSchemeName | undefined;
  selectedDate: string;
}

export const SET_CALENDAR_DATE = "SET_CALENDAR_DATE";
export const SET_COLOR_SCHEME = "SET_COLOR_SCHEME";

interface SetCalendarDateAction {
  type: typeof SET_CALENDAR_DATE;
  payload: string;
}

interface SetColorSchemeAction {
  type: typeof SET_COLOR_SCHEME;
  payload: ColorSchemeName;
}

export type AppActionTypes = SetCalendarDateAction | SetColorSchemeAction;
