import { combineReducers, createStore } from "redux";
import {
  useSelector as useReduxSelector,
  TypedUseSelectorHook,
} from "react-redux";
import { persistStore, persistReducer } from "redux-persist";
import ExpoFileSystemStorage from "redux-persist-expo-filesystem";
import { appReducer } from "./app/reducers";
import { entryReducer } from "./entry/reducers";

const appPersistConfig = {
  key: "app",
  storage: ExpoFileSystemStorage,
  blacklist: ["selectedDate"],
};

const entryPersistConfig = {
  key: "entry",
  storage: ExpoFileSystemStorage,
};

const persistConfig = {
  key: "root",
  storage: ExpoFileSystemStorage,
  blacklist: ["selectedDate"],
};

const rootReducer = combineReducers({
  app: persistReducer(appPersistConfig, appReducer),
  entry: persistReducer(persistConfig, entryReducer),
});

export type RootState = ReturnType<typeof rootReducer>;
export const useSelector: TypedUseSelectorHook<RootState> = useReduxSelector;

export const store = createStore(rootReducer);
// @ts-expect-error
export const persistor = persistStore(store);
