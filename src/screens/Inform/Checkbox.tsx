import React from "react";
import { Feather as Icon } from "@expo/vector-icons";
import { BorderlessTap, Box, Text } from "../../components";
import { useTheme } from "../../components/Theme";

interface CheckboxProps {
  checked?: boolean;
  disabled: boolean;
  name: string;
  onPress: () => void;
  variant: "default" | "primary";
}
const Checkbox = ({ name, checked, disabled, onPress }: CheckboxProps) => {
  const theme = useTheme();
  const color = theme.colors.secondary;
  return (
    <Box paddingBottom="s">
      <BorderlessTap onPress={onPress}>
        <Box
          flexDirection="row"
          alignItems="center"
          opacity={disabled ? 0.5 : 1}
        >
          <Icon
            name={checked ? "check-circle" : "circle"}
            size={20}
            color={color}
          />
          <Text paddingLeft="s">{name}</Text>
        </Box>
      </BorderlessTap>
    </Box>
  );
};

Checkbox.defaultProps = {
  disabled: false,
  variant: "default",
};

export default Checkbox;
