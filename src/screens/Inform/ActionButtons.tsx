import React from "react";
import { Box, ButtonWithIcon } from "../../components";
import { Entry } from "../../redux/entry/types";
import * as MailComposer from "expo-mail-composer";
import { Alert, Clipboard } from "react-native";
import { useLocalisation } from "../../context";
import { readableEntries } from "../../utils";

interface ActionButtonProps {
  entries: Entry[];
}
const ActionButton = ({ entries }: ActionButtonProps) => {
  const text = readableEntries(entries);
  const { t } = useLocalisation();

  const onMail = async () => {
    const isAvailable = await MailComposer.isAvailableAsync();
    if (isAvailable) {
      MailComposer.composeAsync({
        subject: t("inform.email.subject"),
        body: text,
      });
    } else {
      Alert.alert(
        t("inform.email.alert.title"),
        t("inform.email.alert.description"),
        [
          {
            text: "OK",
          },
        ]
      );
    }
  };

  const onClipboard = () => {
    Clipboard.setString(text);
    Alert.alert(t("inform.clipboard.alert.title"), undefined, [
      {
        text: "OK",
      },
    ]);
  };

  return (
    <Box paddingVertical="s">
      <ButtonWithIcon
        onPress={onMail}
        label={t("inform.email.button")}
        variant="primary"
        name="mail"
      />
      <ButtonWithIcon
        onPress={onClipboard}
        label={t("inform.clipboard.button")}
        variant="primary"
        name="copy"
      />
    </Box>
  );
};

export default ActionButton;
