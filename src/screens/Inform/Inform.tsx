import React, { useEffect, useState } from "react";
import { Alert, ScrollView } from "react-native";
import { BorderlessTap, Box, MainButton, Text } from "../../components";
import { useTheme } from "../../components/Theme";
import Checkbox from "./Checkbox";
import * as Contacts from "expo-contacts";
import { AppNavigationProps } from "../../components/Navigation";
import * as SMS from "expo-sms";
import { useLocalisation } from "../../context";
import { isInLastDays } from "../../utils";
import { useSelector } from "../../redux";
import ActionButton from "./ActionButtons";

interface RegisteredContacts extends Contacts.Contact {
  checked?: boolean;
}

interface InformProps {}
const Inform = ({ navigation }: AppNavigationProps<"Inform">) => {
  const theme = useTheme();
  const entries = useSelector((state) => state.entry.list);
  const [unregisteredContacts, setUnregisteredContacts] = useState<string[]>(
    []
  );
  const [registeredContacts, setRegisteredContacts] = useState<
    RegisteredContacts[]
  >([]);
  const { t } = useLocalisation();
  useEffect(() => {
    const unregistered: string[] = [];
    const registered: RegisteredContacts[] = [];
    entries.forEach((entry) => {
      const inLastDays = isInLastDays(14, entry.date);
      if (inLastDays) {
        entry.unregisteredContacts?.forEach((c) => {
          if (!unregistered.find((i) => i === c)) {
            unregistered.push(c);
          }
        });
        entry.registeredContacts?.forEach((c) => {
          if (!registered.find((i) => i.id === c.id)) {
            const newContact: RegisteredContacts = c;
            newContact.checked = true;
            registered.push(c);
          }
        });
      }
    });
    setUnregisteredContacts(unregistered);
    setRegisteredContacts(registered);
  }, []);

  const onSend = async () => {
    const phoneNumbers: string[] = [];
    registeredContacts.forEach((c) => {
      if (c.checked) {
        if (c.phoneNumbers && c.phoneNumbers.length > 0) {
          if (c.phoneNumbers[0].number) {
            phoneNumbers.push(c.phoneNumbers[0].number.toString());
          }
        }
      }
    });
    const isAvailable = await SMS.isAvailableAsync();
    if (isAvailable) {
      SMS.sendSMSAsync(phoneNumbers, t("inform.sms.message"));
    } else {
      Alert.alert("cannot use SMS");
    }
  };

  return (
    <Box flex={1}>
      <ScrollView
        style={{ flex: 1 }}
        contentContainerStyle={{
          padding: theme.spacing.m,
          paddingBottom: theme.spacing.m + 75,
        }}
      >
        <Text>{t("inform.officials")}</Text>
        <ActionButton entries={entries} />
        <Divider />
        <Text>{t("inform.text")}</Text>
        <Box paddingTop="m">
          {registeredContacts.map((i) => {
            const onPress = () => {
              setRegisteredContacts(
                registeredContacts.map((c) =>
                  c.id === i.id ? { ...i, checked: !i.checked } : c
                )
              );
            };
            return (
              <Checkbox
                key={i.id}
                name={i.name}
                checked={i.checked}
                onPress={onPress}
              />
            );
          })}
          {unregisteredContacts.map((i) => {
            return (
              <Box key={i} opacity={0.5}>
                <Checkbox name={i} onPress={() => null} />
              </Box>
            );
          })}
        </Box>
      </ScrollView>
      <MainButton icon="send" onPress={onSend} />
    </Box>
  );
};

const Divider = () => (
  <Box height={2} flex={1} backgroundColor="cardPrimary" marginVertical="s" />
);

export default Inform;
