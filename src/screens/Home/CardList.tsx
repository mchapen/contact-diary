import React, { useLayoutEffect, useRef } from "react";
import { ScrollView } from "react-native";
import { Box, Card, Contact, Text } from "../../components";
import { Feather as Icon } from "@expo/vector-icons";
import { useTheme } from "../../components/Theme";
import BorderlessTap from "../../components/BorderlessTap";
import { useNavigation } from "@react-navigation/native";
import { Entry } from "../../redux/entry/types";
import {
  Transition,
  TransitioningView,
  Transitioning,
} from "react-native-reanimated";
import { sliceArrays } from "../../utils";
import { useWindowDimensions } from "react-native";

const transitionNext = (
  <Transition.Together>
    <Transition.In type="slide-right" delayMs={0} durationMs={300} />
    <Transition.Out type="slide-left" delayMs={0} durationMs={300} />
  </Transition.Together>
);

const transitionPrev = (
  <Transition.Together>
    <Transition.In type="slide-right" delayMs={0} durationMs={300} />
    <Transition.Out type="slide-left" delayMs={0} durationMs={300} />
  </Transition.Together>
);

interface CardListProps {
  entries: Entry[];
}

const CardList = ({ entries }: CardListProps) => {
  const theme = useTheme();
  const navigation = useNavigation();
  const { width } = useWindowDimensions();
  const ref = useRef<TransitioningView>(null);
  useLayoutEffect(() => {
    if (ref.current) {
      ref.current.animateNextTransition();
    }
  }, [entries]);
  return (
    <ScrollView
      style={{ flex: 1 }}
      contentContainerStyle={{
        paddingTop: theme.spacing.s,
        paddingBottom: theme.spacing.m + 75,
      }}
      scrollEnabled={theme.breakpoints.iphone8 < width}
    >
      <Transitioning.View transition={transitionNext} {...{ ref }}>
        <Box paddingTop="s" paddingHorizontal="m">
          {entries.map((item) => {
            const numberOfContacts =
              item.registeredContacts.length + item.unregisteredContacts.length;
            const {
              slicedArr1: registeredContacts,
              slicedArr2: unregisteredContacts,
            } = sliceArrays(
              item.registeredContacts,
              item.unregisteredContacts,
              3
            );
            return (
              <Card key={item.id}>
                <BorderlessTap
                  onPress={() => navigation.navigate("Edit", { item })}
                >
                  <Box
                    flexDirection="row"
                    alignItems="center"
                    justifyContent="space-between"
                    paddingBottom="s"
                  >
                    <Text fontFamily={"RobotoSlab-Light"} fontSize={20}>
                      {item.time}
                    </Text>
                    <Icon
                      name="edit"
                      size={24}
                      color={theme.colors.secondary}
                    />
                  </Box>
                  <Text variant="subtitle" numberOfLines={1}>
                    {item.locationName}
                  </Text>
                  <Divider />
                  {registeredContacts?.map((contact) => (
                    <Contact
                      key={contact.name}
                      registered={contact}
                      paddingTop="s"
                    />
                  ))}
                  {unregisteredContacts?.map((contact) => (
                    <Contact
                      key={contact}
                      unregistered={contact}
                      paddingTop="s"
                    />
                  ))}
                  {numberOfContacts > 3 && (
                    <Text paddingTop="xs" style={{ paddingLeft: 2 }}>
                      ... +{numberOfContacts - 3}
                    </Text>
                  )}
                </BorderlessTap>
              </Card>
            );
          })}
        </Box>
      </Transitioning.View>
    </ScrollView>
  );
};

const Divider = () => (
  <Box height={2} backgroundColor="secondary" marginVertical="s" />
);

export default CardList;
