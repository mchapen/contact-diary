import React, { useState, useRef, useLayoutEffect, useEffect } from "react";
import { ScrollView, useWindowDimensions } from "react-native";
import { Box, FAB, RoundedIcon } from "../../components";
import { AppNavigationProps } from "../../components/Navigation";
import CardList from "./CardList";
import { Calendar, DateObject, LocaleConfig } from "react-native-calendars";
import { Text, useTheme } from "../../components/Theme";
import { useLocalisation } from "../../context";
import moment from "moment";
import { calendarDE, calendarEN, calendarFR } from "../../translations";
import CalendarDay from "./CalendarDay";
import { useSelector } from "../../redux";
import { useDispatch } from "react-redux";
import { setCalendarDate } from "../../redux/app/actions";

const currentDate = moment().format("YYYY-MM-DD");
LocaleConfig.locales.de = calendarDE;
LocaleConfig.locales.en = calendarEN;
LocaleConfig.locales.fr = calendarFR;
const languages = ["de", "en", "fr"];

const Home = ({ navigation }: AppNavigationProps<"Home">) => {
  const theme = useTheme();
  const colorScheme = useSelector((state) => state.app.colorScheme);
  const { width } = useWindowDimensions();
  const scrollRef = useRef(null);
  const { t, locale } = useLocalisation();
  const dispatch = useDispatch();
  LocaleConfig.defaultLocale =
    languages.find((i) => i === locale.split("-")[0]) || "en";
  const entries = useSelector((state) => state.entry.list);
  const [selected, setSelected] = useState<string>(currentDate);
  const onDayPress = (day: DateObject) => {
    setSelected(day.dateString);
    dispatch(setCalendarDate(day.dateString));
  };
  const dotMarkDates: any = [];
  entries.forEach((entry) => {
    dotMarkDates[entry.date] = {
      marked: true,
    };
  });
  const markDates = {
    ...dotMarkDates,
    [selected]: {
      selected: true,
      disableTouchEvent: true,
      selectedColor: theme.colors.calendarPrimary,
      selectedTextColor: theme.colors.mainForeground,
      dotColor: theme.colors.mainForeground,
    },
  };

  useEffect(() => {
    if (scrollRef !== null && scrollRef.current) {
      // @ts-expect-error
      scrollRef.current?.scrollTo({ y: 0, animated: true });
    }
  }, [selected]);

  return (
    <Box flex={1}>
      <ScrollView
        ref={scrollRef}
        style={{ flex: 1 }}
        scrollEnabled={theme.breakpoints.iphone8 >= width}
      >
        <Calendar
          key={`${colorScheme}-${locale}`} // otherwise, the Calendar wouldn't change theme
          onDayPress={onDayPress}
          hideExtraDays
          firstDay={1}
          //maxDate={currentDate}
          markedDates={markDates}
          theme={{
            calendarBackground: theme.colors.mainBackground,
            textSectionTitleColor: theme.colors.mainForeground,
            monthTextColor: theme.colors.mainForeground,
            dayTextColor: theme.colors.mainForeground,
            indicatorColor: theme.colors.calendarPrimary,
            arrowColor: theme.colors.calendarPrimary,
            todayTextColor: theme.colors.calendarPrimary,
            dotColor: theme.colors.calendarPrimary,
            textMonthFontFamily: "Disket-Mono-Bold",
            textDayHeaderFontSize: 20,
            textDayHeaderFontFamily: "Disket-Mono-Regular",
            textDayFontFamily: "RobotoSlab-Regular",
            //textDisabledColor: "red",
          }}
          // @ts-ignore - missing typescript support
          renderHeader={(date) => {
            const header = date.toString("MMMM yyyy");
            const [month, year] = header.split(" ");
            return (
              <Box
                style={{
                  flexDirection: "row",
                }}
              >
                <Text variant="navigationTitle">{`${month}/`}</Text>
                <Text variant="subtitle">{year}</Text>
              </Box>
            );
          }}
          renderArrow={(direction) => (
            <RoundedIcon
              name={direction === "left" ? "chevron-left" : "chevron-right"}
              size={30}
              color="secondary"
              iconRatio={1}
              backgroundColor={undefined}
            />
          )}
          dayComponent={(props) => {
            return (
              <CalendarDay
                onDayPress={onDayPress}
                markDates={markDates}
                {...props}
              />
            );
          }}
        />
        <Box
          height={3}
          backgroundColor="cardPrimary"
          marginTop="m"
          style={{
            marginHorizontal: theme.spacing.m - 4,
          }}
        />
        <CardList
          entries={entries
            .filter((entry) => entry.date === selected)
            .sort((a, b) => (a.time > b.time ? -1 : 1))}
        />
      </ScrollView>
      <FAB />
    </Box>
  );
};

export default Home;
