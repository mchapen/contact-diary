import React, { Dispatch, SetStateAction } from "react";
import { TouchableOpacity } from "react-native-gesture-handler";
import { Box, Text } from "../../components";
import { DateObject, DayComponentProps } from "react-native-calendars";
import moment from "moment";

interface CalendarDayProps extends DayComponentProps {
  onDayPress: (date: DateObject) => void;
  markDates: any;
}

const CalendarDay = ({
  date,
  marking,
  onDayPress,
  markDates,
  ...props
}: CalendarDayProps) => {
  const currentDate = moment().format("YYYY-MM-DD");
  const dateString = date.dateString;
  const dateExistInObject = Object.keys(markDates).find(
    (i) => i === dateString
  );
  const state = dateExistInObject ? markDates[dateString] : undefined;
  const isSelected = state ? state.selected || false : false;
  const isToday = props.state === "today";
  const isMarked = state ? state.marked || false : false;
  const isInFuture = dateString > currentDate;
  const backgroundColor = isSelected ? "secondary" : "mainBackground";
  const borderColor = isSelected || isMarked ? "secondary" : "white";
  const color = isSelected ? "white" : isToday ? "secondary" : "mainForeground";
  return (
    <TouchableOpacity
      onPress={() => onDayPress(date)}
      onLongPress={() => onDayPress(date)}
    >
      <Box
        width={34}
        height={34}
        borderRadius={16}
        borderWidth={1}
        borderColor={borderColor}
        backgroundColor={backgroundColor}
        opacity={isInFuture ? 0.5 : 1}
        justifyContent="center"
        alignItems="center"
      >
        <Text textAlign="center" color={color}>
          {date.day}
        </Text>
      </Box>
    </TouchableOpacity>
  );
};

export default React.memo(CalendarDay);
