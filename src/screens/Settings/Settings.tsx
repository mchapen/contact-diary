import React from "react";
import { Share } from "react-native";
import { Box, Text, MainButton, Anchor } from "../../components";
import { useTheme } from "../../components/Theme";
import { useLocalisation } from "../../context";
import { ScrollView } from "react-native-gesture-handler";
import Constants from "expo-constants";

const size = 75;

interface SettingsProps {}
const Settings = () => {
  const { t, locale, setLocale } = useLocalisation();
  const theme = useTheme();
  const onShare = async () => {
    try {
      const result = await Share.share({
        message: `${t("app.name")}\n${t("app.link")}`,
      });
    } catch (error) {
      //alert(error.message);
    }
  };
  return (
    <Box flex={1}>
      <ScrollView
        style={{ flex: 1 }}
        contentContainerStyle={{
          padding: theme.spacing.m,
          paddingBottom: theme.spacing.m + 75,
        }}
      >
        <Text paddingBottom="l">{t("settings.general.text")}</Text>
        <Text variant="subtitle" paddingBottom="m">
          {t("settings.how.title").toUpperCase()}
        </Text>
        <Text paddingBottom="s">{t("settings.how.yours.title")}</Text>
        <Text paddingBottom="m">{t("settings.how.yours.text")}</Text>
        <Text paddingBottom="s">{t("settings.how.request.title")}</Text>
        <Text paddingBottom="l">{t("settings.how.request.text")}</Text>
        <Text variant="subtitle" paddingBottom="m">
          {t("settings.imprint.title").toUpperCase()}
        </Text>
        <Text paddingBottom="s">{t("settings.imprint.responsible.title")}</Text>
        <Text paddingBottom="m">{t("settings.imprint.responsible.text")}</Text>
        <Text paddingBottom="s">{t("settings.imprint.contact.title")}</Text>
        <Text paddingBottom="l">{t("settings.imprint.contact.text")}</Text>
        <Text variant="subtitle" paddingBottom="m">
          {t("settings.data-policy.title").toUpperCase()}
        </Text>
        <Text paddingBottom="l">{t("settings.data-policy.text")}</Text>
        <Text variant="subtitle" paddingBottom="m">
          {t("settings.disclaimer-of-liability.title").toUpperCase()}
        </Text>
        <Text>{t("settings.disclaimer-of-liability.text")}</Text>
        <Anchor
          href={t("settings.disclaimer-of-liability.link")}
          paddingBottom="l"
        >
          {t("settings.disclaimer-of-liability.link")}
        </Anchor>
        <Box alignItems="center">
          <Text variant="description" paddingBottom="xs">
            v{Constants.manifest.version}
          </Text>
          <Text variant="description" paddingBottom="m">
            © Lindgün GmbH
          </Text>
        </Box>
      </ScrollView>
      <MainButton onPress={onShare} icon="share-2" />
    </Box>
  );
};

export default Settings;
