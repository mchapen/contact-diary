import React from "react";
import { RoundedIconButton } from "../components";
import {
  createStackNavigator,
  StackHeaderTitleProps,
} from "@react-navigation/stack";
import { AppRoutes } from "../components/Navigation";
import { Home } from "./Home";
import { Settings } from "./Settings";
import { Box, Text, useTheme } from "../components/Theme";
import { useLocalisation } from "../context";
import { Edit } from "./Edit";
import { Map } from "./Map";
import { Inform } from "./Inform";

const AppStack = createStackNavigator<AppRoutes>();

const AppNavigator = () => {
  const theme = useTheme();
  const { t } = useLocalisation();
  return (
    <AppStack.Navigator
      screenOptions={({ navigation, route }) => ({
        headerStyle: {
          backgroundColor: theme.colors.primary,
          shadowColor: "transparent",
        },
        headerBackTitleVisible: false,
        headerTintColor: theme.colors.navigationForeground,
        cardStyle: {
          backgroundColor: theme.colors.mainBackground,
        },
        headerBackImage: () => (
          <Box paddingLeft="s">
            <RoundedIconButton
              name="arrow-left-circle"
              size={30}
              iconRatio={0.9}
              color="navigationForeground"
              backgroundColor={undefined}
              onPress={() => navigation.goBack()}
            />
          </Box>
        ),
        headerRight: () => (
          <Box paddingRight="s">
            <RoundedIconButton
              name="info"
              size={30}
              iconRatio={0.9}
              color="navigationForeground"
              backgroundColor={undefined}
              onPress={() => navigation.navigate("Settings")}
            />
          </Box>
        ),
      })}
    >
      <AppStack.Screen
        name="Home"
        component={Home}
        options={{
          headerTitle: (props) => (
            <HeaderTitle
              bold={t("navigation.contact")}
              regular={t("navigation.diary")}
              {...props}
            />
          ),
        }}
      />
      <AppStack.Screen
        name="Settings"
        component={Settings}
        options={{
          headerTitle: (props) => (
            <HeaderTitle bold={t("navigation.settings")} {...props} />
          ),
          headerRight: () => null,
        }}
      />
      <AppStack.Screen
        name="Edit"
        component={Edit}
        options={({ navigation, route }) => ({
          headerTitle: (props) => {
            const item = route.params?.item;
            return (
              <HeaderTitle
                bold={t("navigation.contact")}
                regular={item ? t("navigation.edit") : t("navigation.create")}
                {...props}
              />
            );
          },
        })}
      />
      <AppStack.Screen
        name="Map"
        component={Map}
        options={{
          headerTitle: (props) => (
            <HeaderTitle
              bold={t("navigation.contact")}
              regular={t("navigation.location")}
              {...props}
            />
          ),
        }}
      />
      <AppStack.Screen
        name="Inform"
        component={Inform}
        options={{
          headerTitle: (props) => (
            <HeaderTitle
              bold={t("navigation.contact")}
              regular={t("navigation.inform")}
              {...props}
            />
          ),
        }}
      />
    </AppStack.Navigator>
  );
};

interface HeaderTitleProps extends StackHeaderTitleProps {
  bold: string;
  regular?: string | undefined;
}
const HeaderTitle = ({ bold, regular }: HeaderTitleProps) => (
  <Box flexDirection="row" paddingHorizontal="s">
    <Text
      variant="navigationTitle"
      color="navigationForeground"
      numberOfLines={1}
    >
      {`${bold.toUpperCase()}${regular ? "/" : ""}`}
      <Text
        variant="navigationTitle"
        color="navigationForeground"
        fontFamily="Disket-Mono-Regular"
      >
        {regular?.toUpperCase()}
      </Text>
    </Text>
  </Box>
);

export default AppNavigator;
