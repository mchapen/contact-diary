import React, { useRef, useState } from "react";
import { View } from "react-native";
import { Box } from "../../components";
import MapView, { Marker, MapEvent, Region } from "react-native-maps";
import { AppNavigationProps } from "../../components/Navigation";

const Map = ({ navigation, route }: AppNavigationProps<"Map">) => {
  const { location, setLocation } = route.params;
  const [marker, setMarker] = useState(location);
  const onPress = (event: MapEvent) => {
    setMarker(event.nativeEvent.coordinate);
    setLocation(event.nativeEvent.coordinate);
  };
  const region = useRef<Region>({
    ...location,
    latitudeDelta: 0.03,
    longitudeDelta: 0.03,
  }).current;

  return (
    <Box flex={1}>
      <MapView
        style={{ flex: 1 }}
        region={location ? region : undefined}
        onLongPress={onPress}
        showsUserLocation
      >
        {marker && <Marker coordinate={marker} />}
      </MapView>
    </Box>
  );
};

export default Map;
