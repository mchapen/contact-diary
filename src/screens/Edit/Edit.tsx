import React, { useState } from "react";
import { Box, MainButton } from "../../components";
import { AppNavigationProps } from "../../components/Navigation";
import moment from "moment";
import _uniqueId from "lodash/uniqueId";
import { Entry } from "../../redux/entry/types";
import { useDispatch } from "react-redux";
import { Card, useTheme } from "../../components/Theme";
import LocationBox from "./LocationBox";
import TimeBox from "./TimeBox";
import ContactBox from "./ContactBox";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { useActionSheet } from "@expo/react-native-action-sheet";
import { useLocalisation } from "../../context";
import { addEntry, deleteEntry, updateEntry } from "../../redux/entry/actions";
import { useSelector } from "../../redux";

const createEntry = (date?: string) => ({
  id: _uniqueId(),
  time: moment().format("HH:mm"),
  date: date || moment().format("YYYY-MM-DD"),
  timestamp: Date.now(),
  unregisteredContacts: [],
  registeredContacts: [],
  //location
});

const Edit = ({ navigation, route }: AppNavigationProps<"Edit">) => {
  const item = route.params?.item;
  const theme = useTheme();
  const { t } = useLocalisation();
  const dispatch = useDispatch();
  const selectedDate = useSelector((state) => state.app.selectedDate);
  const [entry, setEntry] = useState<Entry>(item || createEntry(selectedDate));
  const { showActionSheetWithOptions } = useActionSheet();
  const onDelete = () => {
    showActionSheetWithOptions(
      {
        options: [t("actions.cancel"), t("actions.delete")],
        destructiveButtonIndex: 1,
        cancelButtonIndex: 0,
      },
      (buttonIndex) => {
        if (buttonIndex === 0) {
        } else if (buttonIndex === 1) {
          dispatch(deleteEntry(entry));
          navigation.navigate("Home");
        }
      }
    );
  };

  const onSave = () => {
    if (item) {
      dispatch(updateEntry(entry));
    } else {
      dispatch(addEntry(entry));
    }
    navigation.navigate("Home");
  };

  return (
    <Box flex={1}>
      <KeyboardAwareScrollView
        style={{ flex: 1 }}
        contentContainerStyle={{
          paddingHorizontal: theme.spacing.m,
          paddingTop: theme.spacing.m,
          paddingBottom: theme.spacing.m + 75,
        }}
      >
        <Card>
          <TimeBox onDelete={onDelete} {...{ entry }} />
          <Divider />
          <LocationBox {...{ entry }} />
          <Divider />
          <ContactBox {...{ entry, setEntry }} />
        </Card>
      </KeyboardAwareScrollView>
      <MainButton onPress={onSave} icon="check" />
    </Box>
  );
};

const Divider = () => (
  <Box height={1} marginVertical="m" backgroundColor="secondary" />
);

export default Edit;
