import React, { Dispatch, SetStateAction, useEffect, useState } from "react";
import {
  Box,
  Text,
  TextInput,
  ButtonWithIcon,
  Ellipsis,
} from "../../components";
import { useLocalisation } from "../../context";
import * as Location from "expo-location";
import { useNavigation } from "@react-navigation/native";
import { LatLng } from "react-native-maps";
import { Entry } from "../../redux/entry/types";
import * as Linking from "expo-linking";
import { Alert, Platform } from "react-native";

interface LocationBoxProps {
  entry: Entry;
  //setEntry: Dispatch<SetStateAction<Entry>>;
}

const LocationBox = ({ entry }: LocationBoxProps) => {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [locationName, setLocationName] = useState<string | null>(
    entry?.locationName || null
  );
  const [mapLocation, setMapLocation] = useState<LatLng | undefined>(
    entry?.location || undefined
  );
  const { t } = useLocalisation();
  const navigation = useNavigation();
  let currentLocationName: string = "";

  const onLocationPress = async () => {
    let { status } = await Location.requestPermissionsAsync();
    if (status !== "granted") {
      Alert.alert(
        t("actions.location-settings"),
        undefined,
        [
          {
            text: t("actions.cancel"),
            style: "cancel",
          },
          {
            text: t("actions.settings"),
            onPress: () => Linking.openSettings(),
          },
        ],
        { cancelable: false }
      );
    } else {
      setIsLoading(true);
      const currentLocation = await Location.getCurrentPositionAsync({});
      if (currentLocation) {
        const location = {
          latitude: currentLocation.coords.latitude,
          longitude: currentLocation.coords.longitude,
        };
        setMapLocation(location);
      }
      setIsLoading(false);
    }
  };

  useEffect(() => {
    if (mapLocation) {
      (async () => {
        const data = await Location.reverseGeocodeAsync(mapLocation);
        //console.log(data);
        const locationString =
          Platform.OS === "ios"
            ? `${data[0].name}, ${data[0].postalCode} ${data[0].city}`
            : `${data[0].street} ${data[0].name}, ${data[0].postalCode} ${data[0].city}`;
        setLocationName(locationString);
        currentLocationName = locationString;
      })();
    }
  }, [mapLocation]);

  useEffect(() => {
    entry.locationName = locationName || undefined;
    entry.location = mapLocation || undefined;
  }, [locationName]);

  const onChangeText = (text: string) => setLocationName(text);
  const onSubmitEditing = () => console.log("end");
  return (
    <Box>
      <Text variant="title" paddingBottom="s">
        {t("entry.location")}
      </Text>
      <TextInput
        value={locationName || ""}
        placeholder={t("entry.input.location")}
        {...{ onChangeText, onSubmitEditing }}
      />
      <Box flexDirection="row" alignItems="center" paddingTop="s">
        <ButtonWithIcon
          name="map-pin"
          label={t("entry.map-pin")}
          onPress={onLocationPress}
        />
        {isLoading && (
          <Box paddingLeft="xs">
            <Ellipsis />
          </Box>
        )}
      </Box>
      <ButtonWithIcon
        name="map"
        label={t("entry.map")}
        onPress={() =>
          navigation.navigate("Map", {
            location: mapLocation || entry.location,
            setLocation: setMapLocation,
          })
        }
        paddingTop="s"
      />
    </Box>
  );
};

export default LocationBox;
