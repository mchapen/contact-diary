import React, { useState } from "react";
import {
  Box,
  Contact as ContactComponent,
  ContactProps as ContactComponentProps,
  BorderlessTap,
  RoundedIconButton,
  TextInput,
} from "../../components";
import { Feather as Icon } from "@expo/vector-icons";
import { useTheme } from "../../components/Theme";
import * as Contacts from "expo-contacts";
import { useLocalisation } from "../../context";

interface ContactProps extends ContactComponentProps {
  onPress: (contact: Contacts.Contact | string) => void;
  onAddPress: (contactName: string, phoneNumber: string) => void;
}

const Contact = ({ onPress, onAddPress, ...props }: ContactProps) => {
  const theme = useTheme();
  const [edit, setEdit] = useState<boolean>(false);
  const [phoneNumber, setPhoneNumber] = useState<string>("");
  const { t } = useLocalisation();
  return (
    <Box paddingBottom="m">
      <Box
        flexDirection="row"
        justifyContent="space-between"
        alignItems="center"
      >
        <ContactComponent {...props} />
        <Box flexDirection="row">
          {props.unregistered && (
            <Box paddingRight="m">
              <BorderlessTap onPress={() => setEdit((prev) => !prev)}>
                <Icon
                  name="edit-3"
                  size={22}
                  color={theme.colors.secondary}
                  style={{ paddingRight: theme.spacing.s }}
                />
              </BorderlessTap>
            </Box>
          )}
          <BorderlessTap
            onPress={() =>
              onPress(props.registered || props.unregistered || "")
            } // TODO: remove default string ""
          >
            <Icon name="trash-2" size={22} color={theme.colors.secondary} />
          </BorderlessTap>
        </Box>
      </Box>
      {edit && (
        <Box flexDirection="row" paddingTop="m" alignItems="center">
          <TextInput
            style={{ flex: 1, marginRight: theme.spacing.s }}
            keyboardType={"phone-pad"}
            value={phoneNumber}
            placeholder={t("entry.input.phone")}
            placeholderTextColor={theme.colors.mainForeground}
            onChangeText={(text) => setPhoneNumber(text)}
          />
          <RoundedIconButton
            name="check"
            size={30}
            color="mainForeground"
            onPress={() => {
              phoneNumber.trim() !== "" &&
                onAddPress(props.unregistered || "", phoneNumber || "");
            }}
            backgroundColor="secondary"
          />
        </Box>
      )}
    </Box>
  );
};

Contact.defaultProps = {
  onAddPress: () => null,
};

export default Contact;
