import { Contact } from "expo-contacts";
import React from "react";
import { Box, Text } from "../../components";
import * as Contacts from "expo-contacts";
import BorderlessTap from "../../components/BorderlessTap";

interface SuggestionListProps {
  contacts: Contacts.Contact[] | undefined;
  onItemPress: (contact: Contacts.Contact) => void;
}
const SuggestionList = ({ contacts, onItemPress }: SuggestionListProps) => {
  return (
    <Box>
      {contacts &&
        contacts.map((contact) => {
          const onPress = () => onItemPress(contact);
          return (
            <BorderlessTap key={contact.id} onPress={onPress}>
              <Box
                borderBottomWidth={1}
                borderBottomColor="cardPrimary"
                paddingVertical="s"
              >
                <Text>{contact.name}</Text>
              </Box>
            </BorderlessTap>
          );
        })}
    </Box>
  );
};

export default SuggestionList;
