import React, { Dispatch, SetStateAction, useState } from "react";
import { Alert } from "react-native";
import { Box, Text, TextInput } from "../../components";
import { useTheme } from "../../components/Theme";
import { useContacts } from "../../hooks";
import SuggestionList from "./SuggestionList";
import Contact from "./Contact";
import { Entry } from "../../redux/entry/types";
import { useLocalisation } from "../../context";
import * as Contacts from "expo-contacts";

interface ContactBoxProps {
  entry: Entry;
  setEntry: Dispatch<SetStateAction<Entry>>;
}

const ContactBox = ({ entry, setEntry }: ContactBoxProps) => {
  const [contactName, setContactName] = useState<string>("");
  const { t } = useLocalisation();
  const contacts = useContacts(contactName);
  const theme = useTheme();

  const onChangeText = (text: string) => setContactName(text);
  const onSubmitEditing = () => {
    if (contactName.trim() !== "") {
      entry.unregisteredContacts = entry.unregisteredContacts.find(
        (i) => i === contactName
      )
        ? [...entry.unregisteredContacts]
        : [contactName, ...entry.unregisteredContacts];
      setContactName("");
    }
  };
  const onItemPress = (contact: Contacts.Contact) => {
    entry.registeredContacts = entry.registeredContacts.find(
      (i) => i.id === contact.id
    )
      ? [...entry.registeredContacts]
      : [contact, ...entry.registeredContacts];
    setContactName("");
  };
  const onDeletePress = (contact: Contacts.Contact | string) => {
    ContactAlert({
      title: t("actions.delete-contact"),
      description: typeof contact === "string" ? contact : contact.name,
      onSuccessPress: () => {
        if (typeof contact === "string") {
          entry.unregisteredContacts = entry.unregisteredContacts?.filter(
            (i) => i !== contact
          );
        } else {
          entry.registeredContacts = entry.registeredContacts?.filter(
            (i) => i.id !== contact.id
          );
        }
        setEntry({ ...entry });
      },
    });
  };

  const onAddPress = (contactName: string, phoneNumber: string) => {
    const [firstName, lastName] = contactName.split(/\s+(.*)/);
    // @ts-ignore
    const contact: Contacts.Contact = {
      [Contacts.Fields.FirstName]: firstName,
      [Contacts.Fields.LastName]: lastName,
      [Contacts.Fields.PhoneNumbers]: [
        {
          label: t("phoneNumber"),
          number: phoneNumber,
        },
      ],
    };
    ContactAlert({
      title: t("actions.create-contact"),
      description: `${contactName} \n ${phoneNumber}`,
      onSuccessPress: async () => {
        const contactId = await Contacts.addContactAsync(contact);
        const systemContact = await Contacts.getContactByIdAsync(contactId);
        if (systemContact) {
          entry.unregisteredContacts = entry.unregisteredContacts?.filter(
            (i) => i !== contactName
          );
          entry.registeredContacts = entry.registeredContacts
            ? [...entry.registeredContacts, systemContact]
            : [systemContact];
          setEntry({ ...entry });
        }
      },
    });
  };

  return (
    <Box>
      <Text variant="title" paddingBottom="s">
        {t("entry.contacts")}
      </Text>
      <TextInput
        value={contactName}
        placeholder={t("entry.input.contacts")}
        {...{ onChangeText, onSubmitEditing }}
      />
      <SuggestionList contacts={contacts} onItemPress={onItemPress} />
      <Box paddingVertical="m">
        {entry.registeredContacts?.map((contact) => (
          <Contact
            key={contact.name}
            registered={contact}
            onPress={onDeletePress}
          />
        ))}
        {entry.unregisteredContacts?.map((contact) => (
          <Contact
            key={contact}
            unregistered={contact}
            onPress={onDeletePress}
            onAddPress={onAddPress}
          />
        ))}
      </Box>
    </Box>
  );
};

interface ContactAlertProps {
  title: string;
  description?: string;
  onSuccessPress: () => void;
  onCancelPress?: () => void;
}

const ContactAlert = ({
  title,
  description,
  onSuccessPress,
  onCancelPress,
}: ContactAlertProps) =>
  Alert.alert(
    title,
    description,
    [
      {
        text: "Cancel",
        style: "cancel",
        onPress: onCancelPress,
      },
      {
        text: "OK",
        onPress: onSuccessPress,
      },
    ],
    { cancelable: false }
  );

export default ContactBox;
