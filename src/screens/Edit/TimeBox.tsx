import React, { SetStateAction, Dispatch, useEffect, useState } from "react";
import { TextInput } from "react-native";
import { Box, RoundedIconButton, Text } from "../../components";
import { useLocalisation } from "../../context";
import { Entry } from "../../redux/entry/types";
import { useTheme } from "../../components/Theme";
const width = 70;

interface TimeBoxProps {
  entry: Entry;
  setEntry?: Dispatch<SetStateAction<Entry>>;
  onDelete: () => void;
}

const TimeBox = ({ entry, onDelete }: TimeBoxProps) => {
  const { t } = useLocalisation();
  const [defaultHour, defaultMinute] = entry.time.split(":");
  const [isFocused, setIsFocused] = useState(false);
  const [minute, setMinute] = useState(defaultMinute);
  const [hour, setHour] = useState(defaultHour);
  const theme = useTheme();

  const onChangeHour = (text: string) => setHour(text);
  const onChangeMinute = (text: string) => setMinute(text);

  useEffect(() => {
    const newTime = `${hour}:${minute}`;
    entry.time = newTime;
  }, [minute, hour]);

  return (
    <Box>
      <Box
        flexDirection="row"
        justifyContent="space-between"
        alignItems="center"
        paddingBottom="s"
      >
        <Text variant="title">{t("entry.time")}</Text>
        <RoundedIconButton
          name="trash-2"
          size={30}
          iconRatio={0.9}
          color="secondary"
          backgroundColor={undefined}
          onPress={onDelete}
        />
      </Box>
      <Box
        flexDirection="row"
        alignItems="center"
        justifyContent="center"
        width={width}
        padding="s"
        borderRadius={theme.spacing.s}
        borderWidth={1}
        borderColor={isFocused ? "inputFocus" : "mainForeground"}
        backgroundColor="cardPrimary"
      >
        <TextInput
          onFocus={() => setIsFocused(true)}
          onBlur={() => {
            if (hour.length === 0) {
              setHour("00");
            } else if (hour.length === 1) {
              setHour(`0${hour}`);
            }
            setIsFocused(false);
          }}
          keyboardType="number-pad"
          maxLength={2}
          style={[
            {
              color: theme.colors.mainForeground,
              width: (width - 3 * theme.spacing.s) / 2,
              textAlign: "center",
              fontFamily: "RobotoSlab-Light",
            },
          ]}
          value={hour}
          onChangeText={onChangeHour}
        />
        <Box height={11}>
          <Text color="secondary" lineHeight={14}>
            :
          </Text>
        </Box>
        <TextInput
          onFocus={() => setIsFocused(true)}
          onBlur={() => {
            if (minute.length === 0) {
              setMinute("00");
            } else if (minute.length === 1) {
              setMinute(`0${minute}`);
            }
            setIsFocused(false);
          }}
          keyboardType="number-pad"
          maxLength={2}
          style={[
            {
              color: theme.colors.mainForeground,
              width: (width - 2 * theme.spacing.s) / 2,
              textAlign: "center",
              fontFamily: "RobotoSlab-Light",
            },
          ]}
          value={minute}
          onChangeText={onChangeMinute}
        />
      </Box>
    </Box>
  );
};

export default TimeBox;
