import React from "react";
import { ThemeProvider, LoadAssets } from "./src/components";
import { Localize } from "./src/context";
import AppNavigator from "./src/screens";
import { Provider } from "react-redux";
import { persistor, store } from "./src/redux";
import { PersistGate } from "redux-persist/integration/react";
import { ActionSheetProvider } from "@expo/react-native-action-sheet";

const fonts = {
  "Disket-Mono-Bold": require("./assets/fonts/DisketRostype/Disket-Mono-Bold.ttf"),
  "Disket-Mono-Regular": require("./assets/fonts/DisketRostype/Disket-Mono-Regular.ttf"),
  "RobotoSlab-Bold": require("./assets/fonts/RobotoSlab/RobotoSlab-Bold.ttf"),
  "RobotoSlab-ExtraBold": require("./assets/fonts/RobotoSlab/RobotoSlab-ExtraBold.ttf"),
  "RobotoSlab-ExtraLight": require("./assets/fonts/RobotoSlab/RobotoSlab-ExtraLight.ttf"),
  "RobotoSlab-Light": require("./assets/fonts/RobotoSlab/RobotoSlab-Light.ttf"),
  "RobotoSlab-Medium": require("./assets/fonts/RobotoSlab/RobotoSlab-Medium.ttf"),
  "RobotoSlab-Regular": require("./assets/fonts/RobotoSlab/RobotoSlab-Regular.ttf"),
  "RobotoSlab-SemiBold": require("./assets/fonts/RobotoSlab/RobotoSlab-SemiBold.ttf"),
  "RobotoSlab-Thin": require("./assets/fonts/RobotoSlab/RobotoSlab-Thin.ttf"),
};

const assets = [require("./assets/splash.png")];

export default function App() {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <ActionSheetProvider>
          <Localize>
            <ThemeProvider>
              <LoadAssets {...{ fonts, assets }}>
                <AppNavigator />
              </LoadAssets>
            </ThemeProvider>
          </Localize>
        </ActionSheetProvider>
      </PersistGate>
    </Provider>
  );
}
